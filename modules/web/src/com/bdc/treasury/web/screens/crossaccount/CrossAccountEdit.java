package com.bdc.treasury.web.screens.crossaccount;

import com.haulmont.cuba.gui.model.InstanceContainer;
import com.haulmont.cuba.gui.screen.*;
import com.bdc.treasury.entity.CrossAccount;

import javax.inject.Inject;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

@UiController("treasury_CrossAccount.edit")
@UiDescriptor("cross-account-edit.xml")
@EditedEntityContainer("crossAccountDc")
@LoadDataBeforeShow
public class CrossAccountEdit extends StandardEditor<CrossAccount> {
    @Inject
    private InstanceContainer<CrossAccount> crossAccountDc;

    @Subscribe
    public void onBeforeCommitChanges(BeforeCommitChangesEvent event) throws ParseException {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        LocalDateTime now = LocalDateTime.now();
        String currDate = dtf.format(now);
        Date date = new SimpleDateFormat("MM/dd/yyyy").parse(currDate);
        crossAccountDc.getItem().setCreationDate(date);
    }
}