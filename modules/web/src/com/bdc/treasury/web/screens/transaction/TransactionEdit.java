package com.bdc.treasury.web.screens.transaction;

import com.bdc.treasury.entity.CheckerStatus;
import com.bdc.treasury.entity.CrossAccount;
import com.bdc.treasury.entity.Status;
import com.bdc.treasury.entity.Transaction;
import com.bdc.treasury.service.CrossAccountHolidayService;
import com.bdc.treasury.service.UserService;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.Screens;
import com.haulmont.cuba.gui.components.DateField;
import com.haulmont.cuba.gui.components.LookupField;
import com.haulmont.cuba.gui.components.LookupPickerField;
import com.haulmont.cuba.gui.model.DataContext;
import com.haulmont.cuba.gui.model.InstanceContainer;
import com.haulmont.cuba.gui.screen.*;
import com.haulmont.cuba.security.entity.Group;
import com.haulmont.cuba.security.global.UserSession;

import javax.inject.Inject;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@UiController("treasury_Transaction.edit")
@UiDescriptor("transaction-edit.xml")
@EditedEntityContainer("transactionDc")
@LoadDataBeforeShow
public class TransactionEdit extends StandardEditor<Transaction> {

    private final static String CHECKER_RULE = "checker";

    @Inject
    private UserSession userSession;

    @Inject
    private LookupField<String> checkerStatusField;

    @Inject
    private LookupField<String> statusField;

    @Inject
    private InstanceContainer<Transaction> transactionDc;

    @Inject
    private Notifications notifications;

    @Inject
    private UserService userService;

    @Inject
    private CrossAccountHolidayService crossAccountHolidayService;
    @Inject
    private LookupPickerField<CrossAccount> accountField;
    @Inject
    private DateField<LocalDate> valueDateField;


    @Subscribe
    public void onInit(InitEvent event) {

        List<String> checkerList = new ArrayList<>();
        List<String> makerList = new ArrayList<>();
        checkerList.add(CheckerStatus.RETURNED.name());
        checkerList.add(CheckerStatus.VALIDATED.name());
        makerList.add(Status.SAVED.name());
        makerList.add(Status.SUBMITTED.name());
        checkerStatusField.setOptionsList(checkerList);
        statusField.setOptionsList(makerList);
    }


    @Subscribe(target = Target.DATA_CONTEXT)
    public void onPreCommit(DataContext.PreCommitEvent event) {

        Group group = userSession.getUser().getGroup();
        List<String> rolesNames = userService.getUserRoles();

        Transaction transaction = transactionDc.getItem();
        transaction.setGroupID(group.getId());
        LocalDate valueDate = transaction.getValueDate();
        LocalDate now = LocalDate.now();
        if (valueDate.isBefore(now) && !rolesNames.contains(CHECKER_RULE)) {
            event.preventCommit();
            notifications.create().withCaption("Value date must be after current date").show();
        }
        if (transaction.getAccount() != null && transaction.getValueDate() != null) {
            if (!valueDate.isBefore(now) && crossAccountHolidayService.checkHolidaysOfCrossAccount(transaction.getAccount(), transaction.getValueDate())) {
                event.preventCommit();
                notifications.create().withCaption("The value date of this correspondent account ( " + transaction.getAccount().getName() + " ) matches with a holiday day.").show();
            }
        }
    }


}