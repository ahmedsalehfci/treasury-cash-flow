package com.bdc.treasury.web.screens.deals;

import com.haulmont.cuba.gui.screen.*;
import com.bdc.treasury.entity.Deals;

@UiController("treasury_Deals.edit")
@UiDescriptor("deals-edit.xml")
@EditedEntityContainer("dealsDc")
@LoadDataBeforeShow
public class DealsEdit extends StandardEditor<Deals> {
}