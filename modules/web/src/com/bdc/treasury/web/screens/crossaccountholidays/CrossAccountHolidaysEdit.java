package com.bdc.treasury.web.screens.crossaccountholidays;

import com.haulmont.cuba.gui.screen.*;
import com.bdc.treasury.entity.CrossAccountHolidays;

@UiController("treasury_CrossAccountHolidays.edit")
@UiDescriptor("cross-account-holidays-edit.xml")
@EditedEntityContainer("crossAccountHolidaysDc")
@LoadDataBeforeShow
public class CrossAccountHolidaysEdit extends StandardEditor<CrossAccountHolidays> {
}