package com.bdc.treasury.web.screens.crossaccountholidays;

import com.haulmont.cuba.gui.screen.*;
import com.bdc.treasury.entity.CrossAccountHolidays;

@UiController("treasury_CrossAccountHolidays.browse")
@UiDescriptor("cross-account-holidays-browse.xml")
@LookupComponent("crossAccountHolidaysesTable")
@LoadDataBeforeShow
public class CrossAccountHolidaysBrowse extends StandardLookup<CrossAccountHolidays> {
}