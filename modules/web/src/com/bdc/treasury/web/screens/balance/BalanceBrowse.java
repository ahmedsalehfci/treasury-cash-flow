package com.bdc.treasury.web.screens.balance;

import com.bdc.treasury.entity.*;
import com.bdc.treasury.service.UserService;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.LoadContext;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.actions.list.CreateAction;
import com.haulmont.cuba.gui.actions.list.EditAction;
import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.screen.*;
import com.haulmont.cuba.gui.screen.LookupComponent;
import com.haulmont.cuba.security.entity.Group;
import com.haulmont.cuba.security.global.UserSession;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@UiController("treasury_Balance.browse")
@UiDescriptor("balance-browse.xml")
@LookupComponent("balancesTable")
@LoadDataBeforeShow
public class BalanceBrowse extends StandardLookup<Balance> {

//    private final static String CHECKER_RULE = "checker";
//    private final static String MAKER_RULE = "maker";

    private final static String CHECKER_RULE = "Reconciliation-Role-Checker";
    private final static String MAKER_RULE = "Reconciliation-Role-Maker";

    @Inject
    private DataManager dataManager;

    @Named("balancesTable.create")
    private CreateAction<Balance> balancesTableCreate;

    @Named("balancesTable.edit")
    private EditAction<Balance> balancesTableEdit;

    @Inject
    private CollectionContainer<Balance> balancesDc;

    @Inject
    private CollectionContainer<Balance> balancesDc1;

    @Inject
    private UserSession userSession;

    @Inject
    private UserService userService;
    @Inject
    private LookupField<CrossAccount> crossAccount;
    @Inject
    private LookupField<CrossAccount> ValidatedCrossAccount;

    @Inject
    private GroupTable<Balance> balancesTable;
    @Inject
    private Notifications notifications;


    @Subscribe
    public void onInit(InitEvent event) {
        refreshDataContainers();
    }

    @Install(to = "balancesDl", target = Target.DATA_LOADER)
    private List<Balance> balancesDlLoadDelegate(LoadContext<Balance> loadContext) {
        return loadPendingBalances();
    }

    @Install(to = "balancesDl1", target = Target.DATA_LOADER)
    private List<Balance> balancesDl1LoadDelegate(LoadContext<Balance> loadContext) {
        return loadValidatedBalances();
    }


    public void refreshDataContainers() {
        balancesTableEdit.setAfterCloseHandler(transaction -> {
            balancesDc.getMutableItems().clear();
            balancesDc.getMutableItems().addAll(loadPendingBalances());

            balancesDc1.getMutableItems().clear();
            balancesDc1.getMutableItems().addAll(loadValidatedBalances());
        });
        balancesTableCreate.setAfterCloseHandler(transaction -> {
            balancesDc.getMutableItems().clear();
            balancesDc.getMutableItems().addAll(loadPendingBalances());

            balancesDc1.getMutableItems().clear();
            balancesDc1.getMutableItems().addAll(loadValidatedBalances());
        });
    }

    public List<Balance> loadValidatedBalances() {
        return dataManager.load(Balance.class)
                .query("select t from treasury_Balance t where t.status =:stat1 and t.groupID =:groupId")
                .parameter("stat1", CheckerStatus.VALIDATED.name())
                .parameter("groupId", userSession.getUser().getGroup().getId())
                .view("balance-view")
                .list();
    }

    public List<Balance> loadPendingBalances() {
        List<String> rolesNames = userService.getUserRoles();
        Group group = userSession.getUser().getGroup();

        if (rolesNames.contains(CHECKER_RULE)) {

            return dataManager.load(Balance.class)
                    .query("select t from treasury_Balance t where t.status =:stat1 and t.groupID =:groupId")
                    .parameter("stat1", Status.SUBMITTED.name())
                    .parameter("groupId", group.getId())
                    .view("balance-view")
                    .list();

        } else if (rolesNames.contains(MAKER_RULE)) {

            return dataManager.load(Balance.class)
                    .query("select t from treasury_Balance t where (t.status <> :stat1 and t.status <> :stat2) and t.groupID =:groupId")
                    .parameter("stat1", Status.SUBMITTED.name())
                    .parameter("stat2", CheckerStatus.VALIDATED.name())
                    .parameter("groupId", group.getId())
                    .view("balance-view")
                    .list();
        }
        return new ArrayList<>();
    }

    @Subscribe("crossAccount")
    public void onCrossAccountValueChange(HasValue.ValueChangeEvent<CrossAccount> event) {
        List<Balance> balanceList = loadPendingBalances().stream().filter(balance -> {
            if (crossAccount.getValue() != null && crossAccount.getValue().getName() != null)
                return balance.getCrossAccount().getName().equals(crossAccount.getValue().getName());
            return false;
        }).collect(Collectors.toList());
        balancesDc.getMutableItems().clear();
        balancesDc.getMutableItems().addAll(balanceList);
    }

    @Subscribe("ValidatedCrossAccount")
    public void onValidatedCrossAccountValueChange(HasValue.ValueChangeEvent<CrossAccount> event) {
        List<Balance> balanceList = loadValidatedBalances().stream().filter(balance -> {
            if (ValidatedCrossAccount.getValue() != null && ValidatedCrossAccount.getValue().getName() != null)
                return balance.getCrossAccount().getName().equals(ValidatedCrossAccount.getValue().getName());
            return false;
        }).collect(Collectors.toList());
        balancesDc1.getMutableItems().clear();
        balancesDc1.getMutableItems().addAll(balanceList);
    }


    @Subscribe("balances1Table.clearFilter")
    public void onBalancesTableClearFilter(Action.ActionPerformedEvent event) {
        ValidatedCrossAccount.setValue(null);
        balancesDc1.getMutableItems().clear();
        balancesDc1.getMutableItems().addAll(loadValidatedBalances());
    }

    @Subscribe("balancesTable.clearFilter")
    public void onBalancesTableClearFilter1(Action.ActionPerformedEvent event) {
        crossAccount.setValue(null);
        balancesDc.getMutableItems().clear();
        balancesDc.getMutableItems().addAll(loadPendingBalances());
    }

    @Subscribe("VALIDATEDbtn")
    public void onVALIDATEDbtnClick(Button.ClickEvent event) {
        if (balancesTable.getSelected().size() > 0 ) {
            Set<Balance> balanceList = balancesTable.getSelected();
            for (Balance balance : balanceList) {
                balance.setStatus(CheckerStatus.VALIDATED.name());
                balancesDc.getItem().setStatus(CheckerStatus.VALIDATED.name());
                dataManager.commit(balance);
            }
            balancesDc.getMutableItems().clear();
            balancesDc.getMutableItems().addAll(loadPendingBalances());
            balancesDc1.getMutableItems().clear();
            balancesDc1.getMutableItems().addAll(loadValidatedBalances());
        } else {
            message();
        }
    }

    @Subscribe("RETURNEDbtn")
    public void onRETURNEDbtnClick(Button.ClickEvent event) {
        if (balancesTable.getSelected().size() > 0) {
            Set<Balance> balanceList = balancesTable.getSelected();
            for (Balance balance : balanceList) {
                balance.setStatus(CheckerStatus.RETURNED.name());
                balancesDc.getItem(balance).setStatus(CheckerStatus.RETURNED.name());
                dataManager.commit(balance);
            }
            balancesDc.getMutableItems().clear();
            balancesDc.getMutableItems().addAll(loadPendingBalances());
            balancesDc1.getMutableItems().clear();
            balancesDc1.getMutableItems().addAll(loadValidatedBalances());
        } else {
            message();
        }
    }

    @Subscribe("SUBMITTEDbtn")
    public void onSUBMITTEDbtnClick(Button.ClickEvent event) {
        if (balancesTable.getSelected().size() > 0) {
            Set<Balance> balanceList = balancesTable.getSelected();
            for (Balance balance : balanceList) {
                balance.setStatus(Status.SUBMITTED.name());
                balancesTable.setSelected(balance);
                balancesDc.getItem(balance).setStatus(Status.SUBMITTED.name());
                dataManager.commit(balance);
            }
            balancesDc.getMutableItems().clear();
            balancesDc.getMutableItems().addAll(loadPendingBalances());
        } else {
            message();
        }
    }

    public void message() {
        notifications.create().withCaption("You should select one row at least").show();
    }

}