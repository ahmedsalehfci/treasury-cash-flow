package com.bdc.treasury.web.screens.crossaccount;

import com.haulmont.cuba.gui.components.Action;
import com.haulmont.cuba.gui.components.LookupField;
import com.haulmont.cuba.gui.components.TextField;
import com.haulmont.cuba.gui.screen.*;
import com.bdc.treasury.entity.CrossAccount;

import javax.inject.Inject;
import java.math.BigDecimal;

@UiController("treasury_CrossAccount.browse")
@UiDescriptor("cross-account-browse.xml")
@LookupComponent("crossAccountsTable")
@LoadDataBeforeShow
public class CrossAccountBrowse extends StandardLookup<CrossAccount> {

    @Inject
    private TextField<String> bic;
    @Inject
    private TextField<String> corporateGl;
    @Inject
    private TextField<String> retailGl;
    @Inject
    private TextField<Integer> number;
    @Inject
    private LookupField<String> currency;
    @Inject
    private TextField<CrossAccount> crossName;

    @Subscribe("crossAccountsTable.clearFilter")
    public void onCustomerBasesTableClearFilter(Action.ActionPerformedEvent event) {
        number.setValue(null);

        crossName.setValue(null);
        currency.setValue(null);
        retailGl.setValue(null);
        corporateGl.setValue(null);
        bic.setValue(null);
    }
}