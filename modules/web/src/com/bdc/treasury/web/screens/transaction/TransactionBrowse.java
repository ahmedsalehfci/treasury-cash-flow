package com.bdc.treasury.web.screens.transaction;

import com.bdc.treasury.entity.CheckerStatus;
import com.bdc.treasury.entity.CrossAccount;
import com.bdc.treasury.entity.Status;
import com.bdc.treasury.entity.Transaction;
import com.bdc.treasury.service.FilterService;
import com.bdc.treasury.service.UserService;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.LoadContext;
import com.haulmont.cuba.gui.actions.list.CreateAction;
import com.haulmont.cuba.gui.actions.list.EditAction;
import com.haulmont.cuba.gui.components.Action;
import com.haulmont.cuba.gui.components.HasValue;
import com.haulmont.cuba.gui.components.LookupField;
import com.haulmont.cuba.gui.components.VBoxLayout;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.screen.*;
import com.haulmont.cuba.security.global.UserSession;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@UiController("treasury_Transaction.browse")
@UiDescriptor("transaction-browse.xml")
@LookupComponent("transactionsTable")
@LoadDataBeforeShow
public class TransactionBrowse extends StandardLookup<Transaction> {


    private final static String CHECKER_RULE = "checker";
    private final static String MAKER_RULE = "maker";
    private final static String CONTROL_RULE = "control";

    @Inject
    private DataManager dataManager;

    @Inject
    private UserSession userSession;

    @Inject
    private CollectionContainer<Transaction> transactionsDc;

    @Inject
    private CollectionContainer<Transaction> transactionsDc1;

    @Named("transactionsTable.edit")
    private EditAction<Transaction> transactionsTableEdit;

    @Named("transactionsTable.create")
    private CreateAction<Transaction> transactionsTableCreate;

    @Inject
    private UserService userService;
    @Inject
    private FilterService filterService;

    @Named("transactionTapSheet.PendingTap")
    private VBoxLayout pendingTap;
    @Inject
    private LookupField<CrossAccount> crossAccount;
    @Inject
    private LookupField<CrossAccount> validatedCrossAccount;


    @Subscribe
    public void onInit(InitEvent event) {
        refreshDataContainers();

    }

    @Install(to = "transactionsDl1", target = Target.DATA_LOADER)
    private List<Transaction> transactionsDl1LoadDelegate(LoadContext<Transaction> loadContext) {
        return loadValidatedData();
    }

    @Install(to = "transactionsDl", target = Target.DATA_LOADER)
    private List<Transaction> transactionsDlLoadDelegate(LoadContext<Transaction> loadContext) {
        return loadPendingData();
    }

    @Subscribe("crossAccount")
    public void onCrossAccountValueChange(HasValue.ValueChangeEvent<CrossAccount> event) {
        List<Transaction> transactionList = loadPendingData().stream().filter(transaction1 -> {
            if (crossAccount.getValue() != null && crossAccount.getValue().getName() != null)
                return transaction1.getAccount().getName().equals(crossAccount.getValue().getName());
            return false;
        }).collect(Collectors.toList());
        transactionsDc.getMutableItems().clear();
        transactionsDc.getMutableItems().addAll(transactionList);
    }

    @Subscribe("validatedCrossAccount")
    public void onValidatedCrossAccountValueChange(HasValue.ValueChangeEvent<CrossAccount> event) {
        List<Transaction> validatedTransactionList = loadValidatedData().stream().filter(transaction -> {
            if (validatedCrossAccount.getValue() != null && validatedCrossAccount.getValue().getName() != null)
                return transaction.getAccount().getName().equals(validatedCrossAccount.getValue().getName());
            return false;
        }).collect(Collectors.toList());
        transactionsDc1.getMutableItems().clear();
        transactionsDc1.getMutableItems().addAll(validatedTransactionList);
    }


    public void refreshDataContainers() {
        transactionsTableEdit.setAfterCloseHandler(transaction -> {
            transactionsDc.getMutableItems().clear();
            transactionsDc.getMutableItems().addAll(loadPendingData());

            transactionsDc1.getMutableItems().clear();
            transactionsDc1.getMutableItems().addAll(loadValidatedData());

        });
        transactionsTableCreate.setAfterCloseHandler(transaction -> {
            transactionsDc.getMutableItems().clear();
            transactionsDc.getMutableItems().addAll(loadPendingData());
            transactionsDc1.getMutableItems().clear();
            transactionsDc1.getMutableItems().addAll(loadValidatedData());
        });
    }

    public List<Transaction> loadValidatedData() {
        return dataManager.load(Transaction.class)
                .query("select t from treasury_Transaction t where (t.status =:stat1) and t.groupID =:groupId")
                .parameter("stat1", CheckerStatus.VALIDATED.name())
                .parameter("groupId", userSession.getUser().getGroup().getId())
                .view("transaction-view")
                .list();
    }

    public List<Transaction> loadPendingData() {
        List<String> rolesNames = userService.getUserRoles();
        if (rolesNames.contains(CHECKER_RULE)) {
            return dataManager.load(Transaction.class)
                    .query("select t from treasury_Transaction t where (t.status =:stat1) and t.groupID =:groupId")
                    .parameter("stat1", Status.SUBMITTED.name())
                    .parameter("groupId", userSession.getUser().getGroup().getId())
                    .view("transaction-view")
                    .list();

        } else if (rolesNames.contains(MAKER_RULE)) {
            return dataManager.load(Transaction.class)
                    .query("select t from treasury_Transaction t where (t.status <> :stat1 and t.status <> :stat2) and t.groupID =:groupId")
                    .parameter("stat1", Status.SUBMITTED.name())
                    .parameter("stat2", CheckerStatus.VALIDATED.name())
                    .parameter("groupId", userSession.getUser().getGroup().getId())
                    .view("transaction-view")
                    .list();
        } else if (rolesNames.contains(CONTROL_RULE)) {
            return dataManager.load(Transaction.class)
                    .query("select t from treasury_Transaction t")
                    .view("transaction-view")
                    .list();
        }
        return new ArrayList<>();
    }

    @Subscribe("transactionsTable.clearFilter")
    public void onCustomerBasesTableClearFilter(Action.ActionPerformedEvent event) {
        crossAccount.setValue(null);
        transactionsDc.getMutableItems().clear();
        transactionsDc.getMutableItems().addAll(loadPendingData());
    }

    @Subscribe("validatedTransactionsTable.clearFilter")
    public void onValidatedTransactionsTableClearFilter(Action.ActionPerformedEvent event) {
        crossAccount.setValue(null);
        transactionsDc1.getMutableItems().clear();
        transactionsDc1.getMutableItems().addAll(loadValidatedData());
    }

}