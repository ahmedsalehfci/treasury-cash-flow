package com.bdc.treasury.web.screens.treasuryfrontoffice;

import com.bdc.treasury.entity.CheckerStatus;
import com.bdc.treasury.entity.CrossAccount;
import com.bdc.treasury.entity.Transaction;
import com.bdc.treasury.entity.TreasuryFrontOffice;
import com.bdc.treasury.service.FrontOfficeService;

import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.components.Action;
import com.haulmont.cuba.gui.components.DateField;
import com.haulmont.cuba.gui.components.LookupField;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.screen.*;

import javax.inject.Inject;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@UiController("treasury_TreasuryFrontOffice.browse")
@UiDescriptor("treasury-front-office-browse.xml")
@LookupComponent("treasuryFrontOfficesTable")
@LoadDataBeforeShow
public class TreasuryFrontOfficeBrowse extends StandardLookup<TreasuryFrontOffice> {
    @Inject
    private DataManager dataManager;
    @Inject
    private DateField<LocalDate> startDate;
    @Inject
    private DateField<LocalDate> endDate;
    @Inject
    private CollectionContainer<Transaction> transactionsDc;
    @Inject
    private LookupField<CrossAccount> account;
    @Inject
    private Notifications notifications;
    @Inject
    private CollectionContainer<Transaction> transactionsDc1;

    @Inject
    FrontOfficeService frontOfficeService;

    @Subscribe("transactionTable.search")
    public void onTransactionTableSearch(Action.ActionPerformedEvent event) {
        if (account.getValue() != null && startDate.getValue() != null && endDate.getValue() != null) {
            transactionsDc.getMutableItems().clear();
            transactionsDc.getMutableItems().addAll(loadTransactionList());
//            transactionsDc1.getMutableItems().clear();
//            transactionsDc1.getMutableItems().addAll(loadAggregationTransactionList());
        } else {
            notifications.create().withCaption("Must select account and start date and end date").show();
        }

    }

//    public List<Transaction> loadAggregationTransactionList() {
////        return frontOfficeService.loadAggregateTransactionList(account.getValue(),startDate.getValue(),endDate.getValue());
//        List<Transaction> transactionList = dataManager.load(Transaction.class)
//                .query("select SUM(e.dept) from treasury_Transaction e  GROUP BY e.valueDate")
////                .parameter("account", account.getValue())
////                .parameter("startDate", startDate.getValue())
////                .parameter("endDate", endDate.getValue())
////                .parameter("state", CheckerStatus.VALIDATED.name())
//                .view("transaction-view")
//                .list();
//        return  transactionList;
//    }


    public List<Transaction> loadTransactionList() {
        List<Transaction> transactionList = dataManager.load(Transaction.class)
                .query("select e from treasury_Transaction e where e.account =:account and e.valueDate BETWEEN :startDate AND :endDate and e.status = :state")
                .parameter("account", account.getValue())
                .parameter("startDate", startDate.getValue())
                .parameter("endDate", endDate.getValue())
                .parameter("state", CheckerStatus.VALIDATED.name())
                .view("transaction-view")
                .list();
        return transactionList;
    }


}