package com.bdc.treasury.web.screens.deals;

import com.bdc.treasury.entity.CheckerStatus;
import com.bdc.treasury.entity.CrossAccount;
import com.bdc.treasury.entity.Transaction;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.components.Action;
import com.haulmont.cuba.gui.components.DateField;
import com.haulmont.cuba.gui.components.LookupField;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.screen.*;
import com.bdc.treasury.entity.Deals;

import javax.inject.Inject;
import java.util.Date;
import java.util.List;

@UiController("treasury_Deals.browse")
@UiDescriptor("deals-browse.xml")
@LookupComponent("dealsesTable")
@LoadDataBeforeShow
public class DealsBrowse extends StandardLookup<Deals> {
    @Inject
    private DataManager dataManager;
    @Inject
    private DateField<Date> startDate;
    @Inject
    private DateField<Date> endDate;
    @Inject
    private CollectionContainer<Transaction> transactionsDc;
    @Inject
    private LookupField<CrossAccount> account;
    @Inject
    private Notifications notifications;

    @Subscribe("transactionTable.search")
    public void onTransactionTableSearch(Action.ActionPerformedEvent event) {
        if (account.getValue() != null && startDate.getValue() != null && endDate.getValue() != null) {
            transactionsDc.getMutableItems().clear();
            transactionsDc.getMutableItems().addAll(loadTransactionList());
        } else {
            notifications.create().withCaption("Must select account and start date and end date").show();
        }
    }

    public List<Transaction> loadTransactionList() {
        List<Transaction> transactionList = dataManager.load(Transaction.class)
                .query("select e from treasury_Transaction e where e.account =:account and e.valueDate BETWEEN :startDate AND :endDate and e.status = :state")
                .parameter("account", account.getValue())
                .parameter("startDate", startDate.getValue())
                .parameter("endDate", endDate.getValue())
                .parameter("state", CheckerStatus.VALIDATED.name())
                .view("transaction-view")
                .list();
        return transactionList;
    }
}