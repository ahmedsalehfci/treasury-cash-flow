package com.bdc.treasury.web.screens.balance;

import com.bdc.treasury.entity.Balance;
import com.bdc.treasury.entity.CheckerStatus;
import com.bdc.treasury.entity.DepitCreditState;
import com.bdc.treasury.entity.Status;
import com.haulmont.cuba.gui.components.LookupField;
import com.haulmont.cuba.gui.components.TextField;
import com.haulmont.cuba.gui.model.DataContext;
import com.haulmont.cuba.gui.model.InstanceContainer;
import com.haulmont.cuba.gui.screen.*;
import com.haulmont.cuba.security.entity.Group;
import com.haulmont.cuba.security.global.UserSession;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@UiController("treasury_Balance.edit")
@UiDescriptor("balance-edit.xml")
@EditedEntityContainer("balanceDc")
@LoadDataBeforeShow
public class BalanceEdit extends StandardEditor<Balance> {


    @Inject
    private LookupField<String> makerStatus;
    @Inject
    private LookupField<String> checkerStatus;

    @Inject
    UserSession userSession;
    @Inject
    private InstanceContainer<Balance> balanceDc;
    @Inject
    private LookupField<String> depitCreditId;
    @Inject
    private TextField<BigDecimal> depitCreditField;


    @Subscribe
    public void onInit(InitEvent event) {

        // setup drop down list
        List<String> checkerList = new ArrayList<>();
        List<String> makerList = new ArrayList<>();
        List<String> makerDepitCreditStateList = new ArrayList<>();
        makerDepitCreditStateList.add(DepitCreditState.CREDIT.name());
        makerDepitCreditStateList.add(DepitCreditState.DEBIT.name());
        checkerList.add(CheckerStatus.RETURNED.name());
        checkerList.add(CheckerStatus.VALIDATED.name());
        makerList.add(Status.SAVED.name());
        makerList.add(Status.SUBMITTED.name());
        checkerStatus.setOptionsList(checkerList);
        makerStatus.setOptionsList(makerList);
        depitCreditId.setOptionsList(makerDepitCreditStateList);
    }


    @Subscribe(target = Target.DATA_CONTEXT)
    public void onPreCommit(DataContext.PreCommitEvent event) {
        LocalDate now = LocalDate.now();
        Group group = userSession.getUser().getGroup();
        Balance balance = balanceDc.getItem();
        balance.setCreationDate(now);
        balance.setGroupID(group.getId());
        if(depitCreditId.getValue() != null && depitCreditId.getValue().matches(DepitCreditState.CREDIT.name())){
            balanceDc.getItem().setCredit(depitCreditField.getValue());
            balanceDc.getItem().setDept(BigDecimal.ZERO);
        }
        if(depitCreditId.getValue() != null && depitCreditId.getValue().matches(DepitCreditState.DEBIT.name())){
            balanceDc.getItem().setCredit(BigDecimal.ZERO);
            balanceDc.getItem().setDept(depitCreditField.getValue());
        }

    }
}