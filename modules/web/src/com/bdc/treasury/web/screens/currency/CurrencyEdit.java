package com.bdc.treasury.web.screens.currency;

import com.haulmont.cuba.gui.screen.*;
import com.bdc.treasury.entity.Currency;

@UiController("treasury_Currency.edit")
@UiDescriptor("currency-edit.xml")
@EditedEntityContainer("currencyDc")
@LoadDataBeforeShow
public class CurrencyEdit extends StandardEditor<Currency> {
}