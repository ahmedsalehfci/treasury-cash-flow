package com.bdc.treasury.web.screens.treasuryfrontoffice;

import com.haulmont.cuba.gui.screen.*;
import com.bdc.treasury.entity.TreasuryFrontOffice;

@UiController("treasury_TreasuryFrontOffice.edit")
@UiDescriptor("treasury-front-office-edit.xml")
@EditedEntityContainer("treasuryFrontOfficeDc")
@LoadDataBeforeShow
public class TreasuryFrontOfficeEdit extends StandardEditor<TreasuryFrontOffice> {
}