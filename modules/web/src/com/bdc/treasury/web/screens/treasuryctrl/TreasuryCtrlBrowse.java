package com.bdc.treasury.web.screens.treasuryctrl;

import com.bdc.treasury.entity.CrossAccount;
import com.bdc.treasury.entity.Transaction;
import com.bdc.treasury.service.TresuryControlService;
import com.haulmont.cuba.core.global.LoadContext;
import com.haulmont.cuba.gui.components.Action;
import com.haulmont.cuba.gui.components.HasValue;
import com.haulmont.cuba.gui.components.LookupField;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.screen.*;
import com.bdc.treasury.entity.TreasuryCtrl;

import javax.inject.Inject;
import java.text.ParseException;
import java.util.List;
import java.util.stream.Collectors;

@UiController("treasury_TreasuryCtrl.browse")
@UiDescriptor("treasury-ctrl-browse.xml")
@LookupComponent("treasuryCtrlsTable")
@LoadDataBeforeShow
public class TreasuryCtrlBrowse extends StandardLookup<TreasuryCtrl> {

    @Inject
    TresuryControlService tresuryControlService;
    @Inject
    private LookupField<CrossAccount> crossAccount;
    @Inject
    private CollectionContainer<TreasuryCtrl> treasuryCtrlsDc;

    @Install(to = "treasuryCtrlsDl", target = Target.DATA_LOADER)
    private List<TreasuryCtrl> treasuryCtrlsDlLoadDelegate(LoadContext<TreasuryCtrl> loadContext) throws ParseException {

        return loadTreasuryCtrlsData();
    }
    public List<TreasuryCtrl> loadTreasuryCtrlsData() throws ParseException {
        return  tresuryControlService.getTreasuryCtrls();
    }

    @Subscribe("crossAccount")
    public void onCrossAccountValueChange(HasValue.ValueChangeEvent<CrossAccount> event) throws ParseException {
        List<TreasuryCtrl> treasuryCtrlList = loadTreasuryCtrlsData().stream().filter(treasuryCtrl -> {
            if (crossAccount.getValue() != null && crossAccount.getValue().getName() != null)
                return treasuryCtrl.getCorrespondentName().getName().equals(crossAccount.getValue().getName());
            return false;
        }).collect(Collectors.toList());
        treasuryCtrlsDc.getMutableItems().clear();
        treasuryCtrlsDc.getMutableItems().addAll(treasuryCtrlList);
    }

    @Subscribe("treasuryCtrlsTable.clearFilter")
    public void onTreasuryCtrlsTableClearFilter(Action.ActionPerformedEvent event) throws ParseException {
        crossAccount.setValue(null);
        treasuryCtrlsDc.getMutableItems().clear();
        treasuryCtrlsDc.getMutableItems().addAll(loadTreasuryCtrlsData());
    }



}