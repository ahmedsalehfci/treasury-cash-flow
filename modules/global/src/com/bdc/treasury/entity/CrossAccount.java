package com.bdc.treasury.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.cuba.core.entity.annotation.Lookup;
import com.haulmont.cuba.core.entity.annotation.LookupType;

import javax.persistence.*;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

@Table(name = "TREASURY_CROSS_ACCOUNT")
@Entity(name = "treasury_CrossAccount")
@NamePattern("%s|name")
public class CrossAccount extends StandardEntity {
    private static final long serialVersionUID = 8740360586624140247L;

    @Lookup(type = LookupType.DROPDOWN, actions = "lookup")
    @JoinColumn(name = "CURRENCY_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Currency currency;

    @OneToMany(mappedBy = "account")
    private List<Transaction> transactions;

    @OneToMany(mappedBy = "crossAccount")
    private List<Balance> balances;

    @Column(name = "NAME")
    private String name;

    @Column(name = "NUMBER_")
    private Integer number;

    @Column(name = "BIC")
    private String bic;

    @Column(name = "CORP_GL")
    private String corporateGl;

    @Column(name = "RET_GL")
    private String retailGl;


    @Temporal(TemporalType.DATE)
    @Column(name = "CREATION_DATE")
    private Date creationDate;
    @JoinTable(name = "TR_CROS_ACC_HDAY_CROS_ACC_LINK",
            joinColumns = @JoinColumn(name = "CROSS_ACCOUNT_ID"),
            inverseJoinColumns = @JoinColumn(name = "CROSS_ACCOUNT_HOLIDAYS_ID"))
    @ManyToMany
    private List<CrossAccountHolidays> crossAccountHolidayses;

    public List<CrossAccountHolidays> getCrossAccountHolidayses() {
        return crossAccountHolidayses;
    }

    public void setCrossAccountHolidayses(List<CrossAccountHolidays> crossAccountHolidayses) {
        this.crossAccountHolidayses = crossAccountHolidayses;
    }

    public List<Balance> getBalances() {
        return balances;
    }

    public void setBalances(List<Balance> balances) {
        this.balances = balances;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) throws ParseException {
        this.creationDate = creationDate;
    }
    public void setNumber(Integer number) {
        this.number = number;
    }

    public Integer getNumber() {
        return number;
    }

    public String getRetailGl() {
        return retailGl;
    }

    public void setRetailGl(String retailGl) {
        this.retailGl = retailGl;
    }

    public String getCorporateGl() {
        return corporateGl;
    }

    public void setCorporateGl(String corporateGl) {
        this.corporateGl = corporateGl;
    }

    public String getBic() {
        return bic;
    }

    public void setBic(String bic) {
        this.bic = bic;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
}