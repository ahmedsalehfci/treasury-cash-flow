package com.bdc.treasury.entity;

import com.haulmont.chile.core.annotations.MetaProperty;
import com.haulmont.cuba.core.entity.StandardEntity;

import javax.persistence.*;
import java.math.BigDecimal;

@Table(name = "TREASURY_TREASURY_CTRL")
@Entity(name = "treasury_TreasuryCtrl")
public class TreasuryCtrl extends StandardEntity {
    private static final long serialVersionUID = -5162928004500929467L;

    private @MetaProperty
    @Transient
    String currency;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CORRESPONDENT_NAME_ID")
    private CrossAccount correspondentName;

    @Column(name = "BALANCE")
    private BigDecimal balance;

    @Column(name = "NOTE")
    private String note;

    public CrossAccount getCorrespondentName() {
        return correspondentName;
    }

    public void setCorrespondentName(CrossAccount correspondentName) {
        this.correspondentName = correspondentName;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}