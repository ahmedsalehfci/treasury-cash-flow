package com.bdc.treasury.entity;

import com.haulmont.cuba.core.entity.StandardEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Table(name = "TREASURY_DEALS")
@Entity(name = "treasury_Deals")
public class Deals extends StandardEntity {
    private static final long serialVersionUID = -3562191940179981314L;
}