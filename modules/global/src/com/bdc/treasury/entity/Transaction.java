package com.bdc.treasury.entity;

import com.haulmont.chile.core.annotations.MetaProperty;
import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.cuba.core.entity.annotation.Lookup;
import com.haulmont.cuba.core.entity.annotation.LookupType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.UUID;

@Table(name = "TREASURY_TRANSACTION")
@Entity(name = "treasury_Transaction")
@NamePattern("%s|version")
public class Transaction extends StandardEntity {
    private static final long serialVersionUID = -6227899507969818039L;

    @Lookup(type = LookupType.DROPDOWN, actions = "lookup")
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "ACCOUNT_ID")
    private CrossAccount account;

    @NotNull
    @Column(name = "STATUS", nullable = false)
    private String status;


    @Column(name = "GROUP_ID")
    private UUID groupID;

    @Transient
    @MetaProperty
    private Currency currency;


    @NotNull
    @Column(name = "DEPT", nullable = false)
    private BigDecimal dept;

    @NotNull
    @Column(name = "CREDIT", nullable = false)
    private BigDecimal credit;

    @Column(name = "VALUE_DATE")
    private LocalDate valueDate;

    @Column(name = "REFERENCE_NUMBER")
    private String referenceNumber;

    public void setValueDate(LocalDate valueDate) {
        this.valueDate = valueDate;
    }

    public LocalDate getValueDate() {
        return valueDate;
    }


    public UUID getGroupID() {
        return groupID;
    }

    public void setGroupID(UUID groupID) {
        this.groupID = groupID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public BigDecimal getCredit() {
        return credit;
    }

    public void setCredit(BigDecimal credit) {
        this.credit = credit;
    }

    public BigDecimal getDept() {
        return dept;
    }

    public void setDept(BigDecimal dept) {
        this.dept = dept;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public CrossAccount getAccount() {
        return account;
    }

    public void setAccount(CrossAccount account) {
        this.account = account;
    }
}