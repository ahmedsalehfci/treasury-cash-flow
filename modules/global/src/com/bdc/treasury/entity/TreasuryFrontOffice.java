package com.bdc.treasury.entity;

import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.cuba.core.entity.annotation.Lookup;
import com.haulmont.cuba.core.entity.annotation.LookupType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Table(name = "TREASURY_TREASURY_FRONT_OFFICE")
@Entity(name = "treasury_TreasuryFrontOffice")
public class TreasuryFrontOffice extends StandardEntity {
    private static final long serialVersionUID = -4478129137323426635L;


    @Lookup(type = LookupType.DROPDOWN, actions = "lookup")
    @JoinColumn(name = "CURRENCY_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Currency currency;

    @Lookup(type = LookupType.DROPDOWN, actions = "lookup")
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "ACCOUNT_ID")
    private CrossAccount account;

    @Column(name = "VALUE_DATE")
    private LocalDate valueDate;

    @Column(name = "TRANSACTION_NAME")
    private String transactionName;

    @Column(name = "FINAL_BALANCE")
    private BigDecimal finalBalance;

    @Column(name = "OPENING")
    private BigDecimal Opening;


    public BigDecimal getOpening() {
        return Opening;
    }

    public void setOpening(BigDecimal opening) {
        Opening = opening;
    }


    @Column(name = "CLOSING")
    private BigDecimal closing;

    public BigDecimal getClosing() {
        return closing;
    }

    public void setClosing(BigDecimal closing) {
        this.closing = closing;
    }


    public BigDecimal getFinalBalance() {
        return finalBalance;
    }

    public void setFinalBalance(BigDecimal finalBalance) {
        this.finalBalance = finalBalance;
    }


    public String getTransactionName() {
        return transactionName;
    }

    public void setTransactionName(String transactionName) {
        this.transactionName = transactionName;
    }


    public LocalDate getValueDate() {
        return valueDate;
    }

    public void setValueDate(LocalDate valueDate) {
        this.valueDate = valueDate;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public CrossAccount getAccount() {
        return account;
    }

    public void setAccount(CrossAccount account) {
        this.account = account;
    }


}