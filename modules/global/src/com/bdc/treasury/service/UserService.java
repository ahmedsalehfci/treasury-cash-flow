package com.bdc.treasury.service;

import java.util.List;

public interface UserService {
    String NAME = "treasury_UserService";

    public List<String> getUserRoles();
}