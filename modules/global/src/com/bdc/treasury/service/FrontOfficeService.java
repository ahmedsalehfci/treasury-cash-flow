package com.bdc.treasury.service;

import com.bdc.treasury.entity.CrossAccount;
import com.bdc.treasury.entity.Transaction;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

public interface FrontOfficeService {
    String NAME = "treasury_FrontOfficeService";
    public List<Transaction> loadAggregateTransactionList(CrossAccount crossAccount , LocalDate StartDate, LocalDate endDate);
}