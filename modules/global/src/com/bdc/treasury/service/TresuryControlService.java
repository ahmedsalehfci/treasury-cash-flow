package com.bdc.treasury.service;

import com.bdc.treasury.entity.CrossAccount;
import com.bdc.treasury.entity.TreasuryCtrl;

import java.text.ParseException;
import java.util.List;

public interface TresuryControlService {
    String NAME = "treasury_TresuryControlService";
    public List<TreasuryCtrl> getTreasuryCtrls() throws ParseException;

    public List<CrossAccount> getCorrespondentAcount() throws ParseException;
}