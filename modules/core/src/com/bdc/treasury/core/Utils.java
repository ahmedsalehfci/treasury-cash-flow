package com.bdc.treasury.core;

import com.bdc.treasury.entity.*;
import com.bdc.treasury.service.TresuryControlService;
import com.haulmont.cuba.core.global.DataManager;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component(Utils.NAME)
public class Utils {

    public static final String NAME = "treasury_Utils";
    @Inject
    protected DataManager dataManager;

    public Date getCurrentDate() throws ParseException {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        LocalDateTime now = LocalDateTime.now();
        String currDate = dtf.format(now);
        Date date = new SimpleDateFormat("MM/dd/yyyy").parse(currDate);
        return date;
    }

    public TreasuryCtrl mapToTreasuryCtrl(CrossAccount crossAccount) {
        List<Transaction> transactions = crossAccount.getTransactions();
        List<Balance> balances = crossAccount.getBalances();
        BigDecimal totalBalance = BigDecimal.ZERO;
        BigDecimal transactionsBalance=  BigDecimal.ZERO;
        LocalDate now = LocalDate.now();

        for (Transaction transaction : transactions) {
            if(transaction.getStatus().equals(CheckerStatus.VALIDATED.name()) && transaction.getValueDate().equals(now))
                transactionsBalance = transactionsBalance.add(transaction.getDept()).subtract(transaction.getCredit());
        }

        List<Balance> todayBalanceList = balances.stream()
                .filter(balance1 -> balance1.getCreationDate().equals(now) && balance1.getStatus().equals(CheckerStatus.VALIDATED.name()))
                .collect(Collectors.toList());

        if (todayBalanceList.size() > 0 ) {
            Balance openingBalance = todayBalanceList.get(0);
            totalBalance = openingBalance.getBalance().subtract(transactionsBalance);
        }
        TreasuryCtrl treasuryCtrl = dataManager.create(TreasuryCtrl.class);
        treasuryCtrl.setCorrespondentName(crossAccount);
        treasuryCtrl.setBalance(totalBalance);
        treasuryCtrl.setCurrency(crossAccount.getCurrency().getCode());
        return treasuryCtrl;
    }


}