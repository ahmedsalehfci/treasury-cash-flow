-- begin TRE_CROSS_ACCOUNT_HOLIDAYS
create table TRE_CROSS_ACCOUNT_HOLIDAYS (
    ID varchar2(32),
    VERSION number(10) not null,
    CREATE_TS timestamp,
    CREATED_BY varchar2(50 char),
    UPDATE_TS timestamp,
    UPDATED_BY varchar2(50 char),
    DELETE_TS timestamp,
    DELETED_BY varchar2(50 char),
    --
    DAY_ varchar2(255 char),
    MONTH_ varchar2(255 char),
    --
    primary key (ID)
)^
-- end TRE_CROSS_ACCOUNT_HOLIDAYS
-- begin TREASURY_TREASURY_CTRL
create table TREASURY_TREASURY_CTRL (
    ID varchar2(32),
    VERSION number(10) not null,
    CREATE_TS timestamp,
    CREATED_BY varchar2(50 char),
    UPDATE_TS timestamp,
    UPDATED_BY varchar2(50 char),
    DELETE_TS timestamp,
    DELETED_BY varchar2(50 char),
    --
    CORRESPONDENT_NAME_ID varchar2(32),
    BALANCE number(19, 2),
    NOTE varchar2(255 char),
    --
    primary key (ID)
)^
-- end TREASURY_TREASURY_CTRL
-- begin TREASURY_CROSS_ACCOUNT
create table TREASURY_CROSS_ACCOUNT (
    ID varchar2(32),
    VERSION number(10) not null,
    CREATE_TS timestamp,
    CREATED_BY varchar2(50 char),
    UPDATE_TS timestamp,
    UPDATED_BY varchar2(50 char),
    DELETE_TS timestamp,
    DELETED_BY varchar2(50 char),
    --
    CURRENCY_ID varchar2(32),
    NAME varchar2(255 char),
    NUMBER_ number(10),
    BIC varchar2(255 char),
    CORP_GL varchar2(255 char),
    RET_GL varchar2(255 char),
    CREATION_DATE date,
    --
    primary key (ID)
)^
-- end TREASURY_CROSS_ACCOUNT
-- begin TREASURY_CURRENCY
create table TREASURY_CURRENCY (
    ID varchar2(32),
    VERSION number(10) not null,
    CREATE_TS timestamp,
    CREATED_BY varchar2(50 char),
    UPDATE_TS timestamp,
    UPDATED_BY varchar2(50 char),
    DELETE_TS timestamp,
    DELETED_BY varchar2(50 char),
    --
    NAME varchar2(255 char),
    CODE varchar2(255 char) not null,
    SYMBOL varchar2(255 char) not null,
    --
    primary key (ID)
)^
-- end TREASURY_CURRENCY
-- begin TREASURY_TRANSACTION
create table TREASURY_TRANSACTION (
    ID varchar2(32),
    VERSION number(10) not null,
    CREATE_TS timestamp,
    CREATED_BY varchar2(50 char),
    UPDATE_TS timestamp,
    UPDATED_BY varchar2(50 char),
    DELETE_TS timestamp,
    DELETED_BY varchar2(50 char),
    --
    ACCOUNT_ID varchar2(32) not null,
    STATUS varchar2(255 char) not null,
    GROUP_ID varchar2(32),
    DEPT number(19, 2) not null,
    CREDIT number(19, 2) not null,
    VALUE_DATE date,
    REFERENCE_NUMBER varchar2(255 char),
    --
    primary key (ID)
)^
-- end TREASURY_TRANSACTION
-- begin TREASURY_TREASURY_FRONT_OFFICE
create table TREASURY_TREASURY_FRONT_OFFICE (
    ID varchar2(32),
    VERSION number(10) not null,
    CREATE_TS timestamp,
    CREATED_BY varchar2(50 char),
    UPDATE_TS timestamp,
    UPDATED_BY varchar2(50 char),
    DELETE_TS timestamp,
    DELETED_BY varchar2(50 char),
    --
    CURRENCY_ID varchar2(32),
    ACCOUNT_ID varchar2(32) not null,
    VALUE_DATE date,
    TRANSACTION_NAME varchar2(255 char),
    FINAL_BALANCE number(19, 2),
    OPENING number(19, 2),
    CLOSING number(19, 2),
    --
    primary key (ID)
)^
-- end TREASURY_TREASURY_FRONT_OFFICE
-- begin TREASURY_BALANCE
create table TREASURY_BALANCE (
    ID varchar2(32),
    VERSION number(10) not null,
    CREATE_TS timestamp,
    CREATED_BY varchar2(50 char),
    UPDATE_TS timestamp,
    UPDATED_BY varchar2(50 char),
    DELETE_TS timestamp,
    DELETED_BY varchar2(50 char),
    --
    CROSS_ACCOUNT_ID varchar2(32) not null,
    CREATION_DATE date,
    GROUP_ID varchar2(32),
    BALANCE number(19, 2),
    EXLUDED_AMOUNT number(19, 2),
    DEPT number(19, 2),
    CREDIT number(19, 2),
    UNKOWN_FUNDS number(19, 2),
    STATUS varchar2(255 char) not null,
    DEPIT_CREDIT_STATE varchar2(255 char) not null,
    DESCRIPTION varchar2(255 char),
    --
    primary key (ID)
)^
-- end TREASURY_BALANCE
-- begin TREASURY_DEALS
create table TREASURY_DEALS (
    ID varchar2(32),
    VERSION number(10) not null,
    CREATE_TS timestamp,
    CREATED_BY varchar2(50 char),
    UPDATE_TS timestamp,
    UPDATED_BY varchar2(50 char),
    DELETE_TS timestamp,
    DELETED_BY varchar2(50 char),
    --
    primary key (ID)
)^
-- end TREASURY_DEALS
-- begin TR_CROS_ACC_HDAY_CROS_ACC_LINK
create table TR_CROS_ACC_HDAY_CROS_ACC_LINK (
    CROSS_ACCOUNT_ID varchar2(32),
    CROSS_ACCOUNT_HOLIDAYS_ID varchar2(32),
    primary key (CROSS_ACCOUNT_ID, CROSS_ACCOUNT_HOLIDAYS_ID)
)^
-- end TR_CROS_ACC_HDAY_CROS_ACC_LINK
